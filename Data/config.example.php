<?php
return [
    'DATABASE_TYPE' => 'sqlite',
    'DATABASE_FILE' => 'database.db',
    'DATABASE_HOST' => '127.0.0.1',
    'DATABASE_PORT' => '3306',
    'DATABASE_NAME' => '',
    'DATABASE_USER' => '',
    'DATABASE_PASS' => '',

    'SMTP_HOST' => '',
    'SMTP_PORT' => 587,
    'SMTP_SECURE' => 'tls',
    'SMTP_AUTH' => true,
    'SMTP_USER' => '',
    'SMTP_PASS' => '',
    'SMTP_FROM' => '',
    'SMTP_NAME' => '',

    'KEY' => 'thisOneIsSoSecret1',
    'SALT' => 'thisOneIsSoSecret2',

    'JWT_SECRET' => 'thisOneIsSoSecret3',
    'JWT_EXPIRE' => 3600,

    'URL' => 'sfw.oo',
    'DEFAULT_LANG' => 'tr',
];
