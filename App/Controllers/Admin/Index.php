<?php

namespace App\Controllers\Admin;

use Kernel\Controller;
use Kernel\View;
use Kernel\Input;

class Index extends Controller
{

    public function index()
    {
        echo 'dashboard';
    }

}
