# Simple Framework (For custom projects)

# Requires
- PHP 7.3^
- Composer
- MySQL Server
- Nginx or Apache

# Credits
- [FastRoute for route and dispatching](https://github.com/nikic/FastRoute)
- [Medoo database framework](https://github.com/catfan/Medoo)
- [PHPMailer mail class](https://github.com/PHPMailer/PHPMailer)